class ApplicationMailer < ActionMailer::Base
  default from: 'rubygem.hotel@gmail.com'
  layout 'mailer'
end
